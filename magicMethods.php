<?php
class StudentInfo{
    public $just_a_variable="hello BITM";
    public function just_a_method()
    {
       echo "Hello World!";
    }
    public function __construct()
    {
        echo $this->just_a_variable;
    }

    public function __destruct()
    {
        echo "good bye!";
    }
}
$just_a_object= new StudentInfo;
unset($just_a_object);//unsets the object so the preceeding line shows error
$just_a_object->just_a_method();
?>