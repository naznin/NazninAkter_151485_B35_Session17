<?php
class StudentInfo{
   public $std_id="";
    public $std_name="";
    public $std_cgpa=0.00;
    public function set_std_id($std_id){
        $this->std_id=$std_id;
    }
    public function set_std_name($std_name){
        $this->std_name=$std_name;
    }
    public function set_std_cgpa($std_cgpa){
        $this->std_cgpa=$std_cgpa;
    }
   public function get_std_id(){
        return $this->std_id;
    }
    public function get_std_name(){
        return $this->std_name;
    }
    public function get_std_cgpa(){
        return $this->std_cgpa;
    }
    public function showDetails(){
        echo "student ID :".$this->std_id."<br/>";
        echo "student name :".$this->std_name ."<br/>";
        echo "student cgpa :".$this->std_cgpa ."<br/>";
    }
}
$obj=new StudentInfo;
$obj->set_std_id("SEIP151485");
$obj->set_std_name("Naznin");
$obj->set_std_cgpa("3.00");
echo $obj->get_std_id()."<br/>";
echo $obj->get_std_name()."<br/>";
echo $obj->get_std_cgpa()."<br/>";
$obj->showDetails();
?>

